module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.initConfig({
        concat:{
            dist: {
                src: ['src/extended-warranty-service.module.js','src/*.js'],
                dest: 'dist/extended-warranty-service-angularjs-sdk.js'
            }
        }
    });
    grunt.registerTask('default', ['concat']);
};