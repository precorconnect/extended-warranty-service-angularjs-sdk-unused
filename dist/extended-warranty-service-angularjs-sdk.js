(function () {
    angular.module(
        "extendedWarrantyServiceModule",
        []);
})();
(function () {
    angular
        .module("extendedWarrantyServiceModule")
        .provider(
        'extendedWarrantyServiceConfig',
        extendedWarrantyServiceConfigProvider
    );

    function extendedWarrantyServiceConfigProvider() {

        var objectUnderConstruction = {
            setBaseUrl: setBaseUrl,
            $get: $get
        };

        return objectUnderConstruction;

        function setBaseUrl(baseUrl) {
            objectUnderConstruction.baseUrl = baseUrl;
            return objectUnderConstruction;
        }

        function $get() {
            return {
                baseUrl: objectUnderConstruction.baseUrl
            }
        }
    }
})();
(function () {
    angular
        .module('extendedWarrantyServiceModule')
        .factory(
        'extendedWarrantyServiceClient',
        [
            'extendedWarrantyServiceConfig',
            '$http',
            '$q',
            extendedWarrantyServiceClient
        ]);

    function extendedWarrantyServiceClient(extendedWarrantyServiceConfig,
                                           $http,
                                           $q) {

        var lastSubmittedExtendedWarrantyPurchaseOrder;

        return {
            constructCompositeProductInformation: constructCompositeProductInformation,
            constructSimpleProductInformation: constructSimpleProductInformation,
            constructExtendedWarrantyPurchaseOrder: constructExtendedWarrantyPurchaseOrder,
            constructProductCoverageInformation: constructProductCoverageInformation,
            getExtendedWarrantyPurchaseOrder: getExtendedWarrantyPurchaseOrder,
            getCoveragePrice: getCoveragePrice,
            getDiscount: getDiscount,
            listCoveragePlans: listCoveragePlans,
            submitExtendedWarrantyPurchaseOrder: submitExtendedWarrantyPurchaseOrder
        };

        /**
         * Simple product information
         * @typedef {Object} SimpleProductInformation
         * @property {string} serialNumber
         * @property {string} productLine
         */

        /**
         * Constructs SimpleProductInformation
         * @returns {SimpleProductInformation}
         */
        function constructSimpleProductInformation() {
            return {
                serialNumber: null,
                productLine: null
            }
        }

        /**
         * Composite product information
         * @typedef {Object} CompositeProductInformation
         * @property {SimpleProductInformation[]} components
         */

        /**
         * Constructs CompositeProductInformation
         * @returns {CompositeProductInformation}
         */
        function constructCompositeProductInformation() {
            return {
                components: []
            }
        }

        /**
         * An ExtendedWarrantyPurchaseOrder
         * @typedef {Object} ExtendedWarrantyPurchaseOrder
         * @property {ProductCoverageInformation[]} coverages
         * @property {string} [discountRedemptionCode]
         * @property {string} [purchaseOrderNumber]
         * @property {string} sapAccountNumber
         * @property {string} standardWarrantyRegistrationId
         * @property {string} submissionTimestamp
         * @property {string} submissionId
         */

        /**
         * Constructs an ExtendedWarrantyPurchaseOrder
         * @returns {ExtendedWarrantyPurchaseOrder}
         */
        function constructExtendedWarrantyPurchaseOrder() {
            return {
                coverages: [],
                discountRedemptionCode:null,
                purchaseOrderNumber:null,
                sapAccountNumber:null,
                standardWarrantyRegistrationId: null,
                submissionTimestamp: null,
                submissionId: null
            }
        }

        /**
         * Information about a products extended warranty coverage
         * @typedef {Object} ProductCoverageInformation
         * @property {(SimpleProductInformation|CompositeProductInformation)[]} product
         * @property {string} planId
         * @property {number} price
         */

        /**
         * Constructs a ProductCoverageInformation
         * @returns {ProductCoverageInformation}
         */
        function constructProductCoverageInformation() {
            return {
                product: null,
                planId: null,
                price:null
            }
        }

        /**
         * Gets an extended warranty purchase order by its submissionId
         * @param submissionId
         * @returns a promise of {ExtendedWarrantyPurchaseOrder}
         */
        function getExtendedWarrantyPurchaseOrder(submissionId) {
            // use local data if we have it
            if (lastSubmittedExtendedWarrantyPurchaseOrder
                && lastSubmittedExtendedWarrantyPurchaseOrder.submissionId == submissionId) {
                var deferred = $q.defer();
                deferred.resolve(lastSubmittedExtendedWarrantyPurchaseOrder);
                return deferred.promise;
            }
            else {
                // use dummy data for now
                var deferred = $q.defer();
                deferred.resolve(
                    {
                        coverages: [
                            {
                                product: {
                                    components: [
                                        {
                                            serialNumber: "AADSDFDS233233",
                                            productLine: "Treadmill Base - Commercial"
                                        },
                                        {
                                            serialNumber: "FFGDDSSDFS22",
                                            productLine: "Treadmill Display - Commercial"
                                        }
                                    ]
                                },
                                planId: "EW 3/1",
                                price:100
                            },
                            {
                                product: {
                                    serialNumber: "FFGDDSSDFS22",
                                    productLine: "Treadmill Display - Commercial"
                                },
                                planId: "EW 3/3",
                                price:300
                            }
                        ],
                        discountRedemptionCode:"1",
                        purchaseOrderNumber:"2234323",
                        sapAccountNumber:"23423323",
                        standardWarrantyRegistrationId: "234233333",
                        submissionTimestamp: 234234234,
                        submissionId: "23423423"
                    });
                return deferred.promise;

            }

            /*
             var request = $http({
             method: "get",
             url: extendedWarrantyServiceConfig.baseUrl + "/extended-warranty-sales-orders/" + submissionId
             });

             return request
             .then(
             handleSuccess,
             handleError
             );
             */
        }

        /**
         * Gets the price of coverage for a particular coverage plan and one or more productLines
         * @param {string} planId
         * @param {string[]} productLines
         * @returns a promise of {number}
         */
        function getCoveragePrice(planId,
                                  productLines) {
            var price;
            if ("EW 3/1" == planId) {
                price = 100;
            }
            else if ("EW 3/2" == planId) {
                price = 200;
            }
            else if ("EW 3/3" == planId) {
                price = 300;
            }
            else {
                price = 100;
            }

            // use dummy data for now
            var deferred = $q.defer();
            deferred.resolve(price);
            return deferred.promise;
        }

        /**
         * A discount based on taking off a flat amount
         * @typedef {Object} AmountDiscount
         * @property {number} amount
         * @property {number} validFromTimestamp
         * @property {number} validToTimestamp
         * @property {string} redemptionCode
         * @property {number} [redeemedAtTimestamp] - the unix timestamp at which the discount was redeemed
         * @property {string} [redeemedWithPONumber] - the purchase order number with which the discount was redeemed
         */

        /**
         * A discount based on taking off a fixed percentage
         * @typedef {Object} PercentDiscount
         * @property {number} percent
         * @property {number} validFromTimestamp
         * @property {number} validToTimestamp
         * @property {string} redemptionCode
         * @property {number} [redeemedAtTimestamp] - the unix timestamp at which the discount was redeemed
         * @property {string} [redeemedWithPONumber] - the purchase order number with which the discount was redeemed
         */

        /**
         * Gets a discount by its redemption code
         * @param {string} redemptionCode
         * @returns a promise of {(AmountDiscount|PercentDiscount)}
         */
        function getDiscount(redemptionCode) {
            // use dummy data for now

            if(3 == redemptionCode){
                var deferred = $q.defer();
                deferred.reject("invalid redemption code");
                return deferred.promise;
            }

            var discount;
            // amount based
            if(0 == redemptionCode){
                discount = {
                    amount: 200,
                        validFromTimestamp: 1432939149,
                    validToTimestamp: 2432939149,
                    redemptionCode: redemptionCode
                }
            }
            // percent based
            else if(1 == redemptionCode){
                discount = {
                    percent: 10,
                    validFromTimestamp: 1432939149,
                    validToTimestamp: 2432939149,
                    redemptionCode: redemptionCode
                }
            }
            // already redeemed
            else if(2 == redemptionCode){
                discount = {
                    amount: 200,
                    validFromTimestamp: 1432939149,
                    validToTimestamp: 2432939149,
                    redemptionCode: redemptionCode,
                    redeemedAtTimestamp:1432939150,
                    redeemedWithPONumber:"23423322AX"
                }
            }
            // expired
            else{
                discount = {
                    amount: 200,
                    validFromTimestamp: 1432939149,
                    validToTimestamp: 1432939150,
                    redemptionCode: redemptionCode
                }
            }
            var deferred = $q.defer();
            deferred.resolve(discount);
            return deferred.promise;
        }

        /**
         * A coverage plan
         * @typedef {Object} CoveragePlan
         * @property {string} submissionId
         */

        /**
         * Lists all coverage plans
         * @returns a promise of {CoveragePlan[]}
         */
        function listCoveragePlans() {
            // use dummy data for now
            var deferred = $q.defer();
            deferred.resolve([
                {
                    id: "EW 3/1"
                },
                {
                    id: "EW 3/2"
                },
                {
                    id: "EW 3/3"
                }
            ]);
            return deferred.promise;
        }

        /**
         * Submits an extended warranty purchase order
         * @param {ExtendedWarrantyPurchaseOrder} extendedWarrantyPurchaseOrder
         * @returns a promise of {string} - a submission submissionId
         */
        function submitExtendedWarrantyPurchaseOrder(extendedWarrantyPurchaseOrder) {
            // use dummy data for now
            var deferred = $q.defer();
            extendedWarrantyPurchaseOrder.submissionTimestamp = Math.floor(new Date().getTime() / 1000);
            extendedWarrantyPurchaseOrder.submissionId = new Date().getTime();
            lastSubmittedExtendedWarrantyPurchaseOrder = extendedWarrantyPurchaseOrder;
            deferred.resolve((new Date().getTime()));
            return deferred.promise;

            /*var request = $http({
             method: "post",
             url: extendedWarrantyServiceConfig.baseUrl + "/extended-warranty-sales-orders",
             data: extendedWarrantyPurchaseOrder
             });

             return request
             .then
             (
             handleSuccess,
             handleError
             )
             .then
             (
             function (submissionId) {
             // set submissionId and timestamp and store for confirmation page
             extendedWarrantyPurchaseOrder.submissionId = submissionId;

             extendedWarrantyPurchaseOrder.submissionTimestamp =
             Math.floor(new Date().getTime()/1000);

             lastSubmittedExtendedWarrantyPurchaseOrder = extendedWarrantyPurchaseOrder;
             return submissionId;
             },
             function (reason) {
             return $q.reject(reason);
             }
             );*/
        }

        function handleError(response) {
            if (
                !angular.isObject(response.data) || !response.data.message
            ) {

                return ( $q.reject("An unknown error occurred.") );

            }

            // Otherwise, use expected error message.
            return ( $q.reject(response.data.message) );

        }

        function handleSuccess(response) {

            return ( response.data );

        }
    }
})();
