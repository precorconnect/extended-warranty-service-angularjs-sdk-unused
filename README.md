## Description
An AngularJS Module implementing common use cases encountered when integrating AngularJS apps
 with the precorconnect extended warranty service.

## UseCases

#####constructCompositeProductInformation
Constructs CompositeProductInformation

#####constructSimpleProductInformation
Constructs SimpleProductInformation

#####constructExtendedWarrantyPurchaseOrder
Constructs an ExtendedWarrantyPurchaseOrder

#####constructProductCoverageInformation
Constructs a ProductCoverageInformation

#####getExtendedWarrantyPurchaseOrder
Gets an extended warranty purchase order by its submissionId

#####getCoveragePrice
Gets the price of coverage for a particular planId and one or more productLines

#####getDiscount
Gets a discount by its redemption code

#####listCoveragePlans
Lists all extended warranty coverage plans

#####submitExtendedWarrantyPurchaseOrder
Submits an extended warranty purchase order

## Installation
add as bower dependency

```shell
bower install https://bitbucket.org/precorconnect/extended-warranty-service-angularjs-sdk.git --save
```
include in view
```html
<script src="bower-components/angular/angular.js"></script>
<script src="bower-components/extended-warranty-service-angularjs-sdk/dist/extended-warranty-service-angularjs-sdk.js"></script>
```
configure
see below.

## Configuration
####Properties
| Name (* denotes required) | Description |
|------|-------------|
| baseUrl* | The base url of the extended warranty service. |

#### Example
```js
angular.module(
        "app",
        ["extendedWarrantyServiceModule"])
        .config(
        [
            "extendedWarrantyServiceConfigProvider",
            appConfig
        ]);

    function appConfig(extendedWarrantyServiceConfigProvider) {
        extendedWarrantyServiceConfigProvider
            .setBaseUrl("@@extendedWarrantyServiceBaseUrl");
    }
```